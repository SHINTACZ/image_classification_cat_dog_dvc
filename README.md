# Image_Classification_Cat_Dog_DVC


## Initialize DVC in repo
`dvc init`
`git status`
`git add -A`
`git commit -m "Initialize DVC"`
`git push`

## Add Dataset and scripts
`dvc add data history_accuracy.png history.csv history_loss.png model.h5 train.ipynb train.py`
`git add data.dvc history_accuracy.png.dvc history.csv.dvc history_loss.png.dvc model.h5.dvc train.ipynb.dvc train.py.dvc .gitignore`
`git commit -m "First model, trained with 500 images"`
`git tag -a "v1.0" -m "model v1.0, 500 images"`
`git push`

`dvc add data history_accuracy.png history.csv history_loss.png model.h5 train.ipynb train.py`
`git add data.dvc history_accuracy.png.dvc history.csv.dvc history_loss.png.dvc model.h5.dvc train.ipynb.dvc train.py.dvc .gitignore`
`git commit -m "First model, trained with 500 images"`
`git tag -a "v1.0" -m "model v1.0, 500 images"`
`git push`

